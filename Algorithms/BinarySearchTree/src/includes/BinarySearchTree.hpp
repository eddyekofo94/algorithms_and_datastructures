#pragma once
#include <iostream>
#include <vector>

#include "Node.hpp"

class BinarySearchTree
{
public:
    BinarySearchTree();
    void insert(int iValue);
    int search(const int& iValue);
    int deleteValue(const int& iValue);
    void printPreOrdered();
    void printOrdered();
    void printPostOrdered();
    std::vector<int> preOrderedBST();
    std::vector<int> sortedBST();
    ~BinarySearchTree();

private:
    void insert(const int& iValue, const std::shared_ptr<Node>& iNode);
    std::shared_ptr<Node> search(const int& iValue,
                                 std::shared_ptr<Node> iNode);
    std::shared_ptr<Node> deleteValue(const int& iValue,
                                      const std::shared_ptr<Node>& iNode);
    void sortedBST(const std::shared_ptr<Node>& iNode);
    void preOrderedBST(const std::shared_ptr<Node>& iNode);
    std::shared_ptr<Node> rootNode = nullptr;
    void printOrdered(const std::shared_ptr<Node>& iNode);
    void printPreOrdered(std::shared_ptr<Node> iNode);
    void printPostOrdered(const std::shared_ptr<Node>& iNode);
    std::vector<int> bstCollection{};
};
