#pragma once
#include <memory>

struct Node
{
    Node();
    std::shared_ptr<Node> leftPtr, rightPtr;
    explicit Node(const int &iValue);
    ~Node();
    friend std::ostream &operator<<(std::ostream &os, const Node &node);
    size_t value;
};
