#include "BinarySearchTree.hpp"

#include <iterator>
#include <memory>

namespace {
bool leafNode(const std::shared_ptr<Node>& iNode)
{
    if (not iNode) return false;
    if ((not iNode->leftPtr) && (not iNode->rightPtr))
    {
        return true;
    }
    return false;
}

std::shared_ptr<Node> nodeHasOneChild(const std::shared_ptr<Node>& iNode)
{
    if (not iNode) return nullptr;
    if ((iNode->leftPtr) && (not iNode->rightPtr))
    {
        return iNode->leftPtr;
    }
    else if ((iNode->rightPtr) && (not iNode->leftPtr))
    {
        return iNode->rightPtr;
    }
    return nullptr;
}

std::shared_ptr<Node> getParentNode(const std::shared_ptr<Node>& iNode,
                                    std::shared_ptr<Node> iRootNode)
{
    if (not iRootNode) return nullptr;
    if (iRootNode->leftPtr && iRootNode->leftPtr->value == iNode->value)
    {
        return iRootNode;
    }
    if (iRootNode->rightPtr && iRootNode->rightPtr->value == iNode->value)
    {
        return iRootNode;
    }
    if (iRootNode->leftPtr && iRootNode->leftPtr->value > iNode->value)
    {
        return getParentNode(iNode, iRootNode->leftPtr);
    }
    if (iRootNode->rightPtr && iRootNode->rightPtr->value > iNode->value)
    {
        return getParentNode(iNode, iRootNode->leftPtr);
    }
    return nullptr;
}

std::shared_ptr<Node> findMinimumNode(std::shared_ptr<Node> iNode)
{
    while (iNode->leftPtr != nullptr)
    {
        iNode = iNode->leftPtr;
    }
    return iNode;
}
}  // namespace

BinarySearchTree::BinarySearchTree()
{
    std::cout << "BST created" << std::endl;
}

BinarySearchTree::~BinarySearchTree() = default;

void BinarySearchTree::insert(const int iValue)
{
    if (rootNode == nullptr)
    {
        rootNode = std::make_shared<Node>(iValue);
    }
    insert(iValue, rootNode);
}

void BinarySearchTree::insert(const int& iValue,
                              const std::shared_ptr<Node>& iNode)
{
    if (iNode->value < iValue)
    {
        if (iNode->rightPtr == nullptr)
        {
            iNode->rightPtr = std::make_shared<Node>(iValue);
            return;
        }
        insert(iValue, iNode->rightPtr);
    }
    if (iNode->value > iValue)
    {
        if (iNode->leftPtr == nullptr)
        {
            iNode->leftPtr = std::make_shared<Node>(iValue);
            return;
        }
        insert(iValue, rootNode->leftPtr);
    }
}

int BinarySearchTree::deleteValue(const int& iValue)
{
    if (rootNode)
    {
        if (not search(iValue, rootNode))
        {
            return -1;
        }

        return deleteValue(iValue, rootNode)->value;
    }
    return -1;
}

std::shared_ptr<Node> BinarySearchTree::deleteValue(
    const int& iValue, const std::shared_ptr<Node>& iNode)
{
    auto deletableNode = search(iValue, iNode);

    if (not deletableNode)
    {
        return nullptr;
    }

    auto parentNode = getParentNode(deletableNode, iNode);
    auto returnNode = std::make_shared<Node>(iValue);
    auto hasOneChildNode = nodeHasOneChild(deletableNode);

    if (leafNode(deletableNode))
    {
        if (parentNode->leftPtr->value == deletableNode->value)
        {
            parentNode->leftPtr = nullptr;
            return returnNode;
        }
        if ((deletableNode) &&
            parentNode->rightPtr->value == deletableNode->value)
        {
            parentNode->rightPtr = nullptr;
            return returnNode;
        }
    }
    else if (hasOneChildNode)
    {
        if (parentNode->leftPtr == deletableNode)
        {
            parentNode->leftPtr = hasOneChildNode;
            deletableNode = nullptr;
            return returnNode;
        }
        else if (parentNode->rightPtr == deletableNode)
        {
            parentNode->rightPtr = hasOneChildNode;
            deletableNode = nullptr;
            return returnNode;
        }
    }
    else
    {
        auto minNode = findMinimumNode(deletableNode);
        parentNode = getParentNode(minNode, deletableNode);
        deletableNode->value = minNode->value;
        parentNode->leftPtr = nullptr;
        return returnNode;
    }
    return returnNode;
}

int BinarySearchTree::search(const int& iValue)
{
    if (rootNode) return search(iValue, rootNode)->value;
    return -1;
}

std::shared_ptr<Node> BinarySearchTree::search(const int& iValue,
                                               std::shared_ptr<Node> iNode)
{
    if (iValue == iNode->value) return iNode;

    if ((iValue < iNode->value) && (iNode->leftPtr))
    {
        return search(iValue, iNode->leftPtr);
    }

    if ((iValue > iNode->value) && (iNode->rightPtr))
    {
        return search(iValue, iNode->rightPtr);
    }
    return nullptr;
}

void BinarySearchTree::printOrdered()
{
    if (rootNode) printOrdered(rootNode);
}

void BinarySearchTree::printOrdered(const std::shared_ptr<Node>& iNode)
{
    if (not iNode) return;
    printOrdered(iNode->leftPtr);
    std::cout << iNode->value << " ";
    printOrdered(iNode->rightPtr);
}

std::vector<int> BinarySearchTree::sortedBST()
{
    if (rootNode) sortedBST(rootNode);
    return bstCollection;
}

void BinarySearchTree::sortedBST(const std::shared_ptr<Node>& iNode)
{
    if (not iNode) return;
    sortedBST(iNode->leftPtr);
    bstCollection.push_back(iNode->value);
    sortedBST(iNode->rightPtr);
}

std::vector<int> BinarySearchTree::preOrderedBST()
{
    if (rootNode) preOrderedBST(rootNode);
    return bstCollection;
}

void BinarySearchTree::preOrderedBST(const std::shared_ptr<Node>& iNode)
{
    if (not iNode) return;
    bstCollection.push_back(iNode->value);
    preOrderedBST(iNode->leftPtr);
    preOrderedBST(iNode->rightPtr);
}
void BinarySearchTree::printPreOrdered()
{
    if (not rootNode) return;
    printPreOrdered(rootNode);
}

void BinarySearchTree::printPreOrdered(std::shared_ptr<Node> iNode)
{
    if (not iNode) return;
    std::cout << iNode->value << " ";
    printPostOrdered(iNode->leftPtr);
    printPostOrdered(iNode->rightPtr);
}

void BinarySearchTree::printPostOrdered()
{
    if (not rootNode) return;
    printPostOrdered(rootNode);
}

void BinarySearchTree::printPostOrdered(const std::shared_ptr<Node>& iNode)
{
    if (not iNode) return;
    printPreOrdered(iNode->leftPtr);
    printPreOrdered(iNode->rightPtr);
    std::cout << iNode->value << " ";
}

std::ostream& operator<<(std::ostream& os, const Node& node)
{
    os << node.value;
    return os;
}
