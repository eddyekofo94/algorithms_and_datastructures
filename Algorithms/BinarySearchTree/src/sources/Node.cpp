#include "Node.hpp"

#include <iostream>

Node::Node() = default;

Node::Node(const int& iValue)
{
    this->value = iValue;
    this->leftPtr = nullptr;
    this->rightPtr = nullptr;
}

Node::~Node() = default;
