#include <gtest/gtest.h>

#include <includes/BinarySearchTree.hpp>

namespace test {
class BinarySearchTreeTestSuite : public ::testing::Test
{
public:
    BinarySearchTreeTestSuite() : tree(std::make_shared<BinarySearchTree>())
    {
        insertValues();
    }
    void insertValues()
    {
        for (auto i : kInsertValues)
        {
            tree->insert(i);
        }
    }

protected:
    std::vector<int> kInsertValues{3, 2, 4, 8, 1, 9, 5};
    std::shared_ptr<BinarySearchTree> tree;
};
TEST_F(BinarySearchTreeTestSuite, deleteTwoNodeWithChildren)
{
    std::vector<int> tempList{5, 3, 1, 4, 7, 6, 10, 8, 12};
    BinarySearchTree tempTree;
    for (int i : tempList)
    {
        tempTree.insert(i);
    }
    EXPECT_EQ(7, tempTree.deleteValue(7));
    EXPECT_EQ(10, tempTree.deleteValue(10));
    std::vector<int> orderedList{1, 3, 4, 5, 6, 8, 12};

    EXPECT_EQ(orderedList, tempTree.sortedBST());
}
// #if 0
TEST_F(BinarySearchTreeTestSuite, deleteNodeWithChildren)
{
    EXPECT_EQ(8, tree->deleteValue(8));

    std::vector<int> orderedList{1, 2, 3, 4, 5, 9};
    EXPECT_EQ(orderedList, tree->sortedBST());
}

TEST_F(BinarySearchTreeTestSuite, deleteNodeWithChild)
{
    EXPECT_EQ(2, tree->deleteValue(2));
    EXPECT_EQ(4, tree->deleteValue(4));

    std::vector<int> orderedList{1, 3, 5, 8, 9};
    EXPECT_EQ(orderedList, tree->sortedBST());
}

TEST_F(BinarySearchTreeTestSuite, deleteLeafNode)
{
    EXPECT_EQ(1, tree->deleteValue(1));
    std::vector<int> orderedList{2, 3, 4, 5, 8, 9};
    EXPECT_EQ(orderedList, tree->sortedBST());
}

TEST_F(BinarySearchTreeTestSuite, search)
{
    EXPECT_EQ(9, tree->search(9));
    EXPECT_EQ(1, tree->search(1));
}

TEST_F(BinarySearchTreeTestSuite, deleteNonLeafNode)
{
    EXPECT_EQ(4, tree->deleteValue(4));
    std::vector<int> orderedList{1, 2, 3, 5, 8, 9};
    EXPECT_EQ(orderedList, tree->sortedBST());
}

TEST_F(BinarySearchTreeTestSuite, orderedBST)
{
    std::vector<int> orderedList{1, 2, 3, 4, 5, 8, 9};
    EXPECT_EQ(orderedList, tree->sortedBST());
}

TEST_F(BinarySearchTreeTestSuite, preOrderedBST)
{
    std::vector<int> preOrderedList{3, 2, 1, 4, 8, 5, 9};
    EXPECT_EQ(preOrderedList, tree->preOrderedBST());
}

TEST_F(BinarySearchTreeTestSuite, preOrderedPrint)
{
    std::cout << "Print pre-ordered tree\n";
    tree->printPreOrdered();
    std::cout << '\n';
}

TEST_F(BinarySearchTreeTestSuite, orderedPrint)
{
    std::cout << "Print ordered tree\n";
    tree->printOrdered();
    std::cout << '\n';
}

TEST_F(BinarySearchTreeTestSuite, postOrderedPrint)
{
    std::cout << "Print post-ordered tree\n";
    tree->printPostOrdered();
    std::cout << '\n';
}

// #endif
}  // namespace test
