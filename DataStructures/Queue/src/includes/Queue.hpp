#include <vector>

template <class T>
class Queue
{
public:
    Queue() = default;
    ~Queue() = default;
    void enqueue(T iValue);
    auto dequeue() -> T;
    auto peek() -> T;
    bool isEmpty();
    void print();

private:
    std::vector<T> queue_{};
    int front_, rear_ = -1;
    void cleanUp();
};
