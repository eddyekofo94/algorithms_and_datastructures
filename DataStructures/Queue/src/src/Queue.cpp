#include "Queue.hpp"

#include <iostream>

template <class T>
void Queue<T>::enqueue(T iValue)
{
    if (front_ == -1)
    {
        front_ = 0;
    }
    else
    {
        rear_++;
        queue_.push_back(iValue);
    }
}

template <class T>
auto Queue<T>::dequeue() -> T
{
    auto response = 0;
    if (isEmpty())
    {
        throw std::invalid_argument("The Queue is empty!");
    }
    else
    {
        response = queue_.at(front_);
        front_++;
        if (isEmpty())
        {
            std::cout << "vector now empty!!" << '\n';
            cleanUp();
        }
    }
    return response;
}
template <class T>
bool Queue<T>::isEmpty()
{
    return (rear_ == -1 || front_ > rear_);
}

template <class T>
auto Queue<T>::peek() -> T
{
    return queue_.front();
}
template <class T>
void Queue<T>::cleanUp()
{
    queue_.clear();
    queue_.shrink_to_fit();
    front_ = -1;
    rear_ = -1;
}
template <class T>
void Queue<T>::print()
{
    if (isEmpty())
    {
        throw std::invalid_argument("The Queue is empty!");
    }
    for (int i = front_; i < rear_; ++i)
    {
        std::cout << queue_.at(i) << " ";
    }
}

template class Queue<int>;