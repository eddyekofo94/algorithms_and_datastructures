#include <gtest/gtest.h>

#include "includes/Queue.hpp"

namespace test {

class QueueTestSuite : public ::testing::Test
{
public:
    QueueTestSuite() : queue_(std::make_shared<Queue<int>>()) {}
    ~QueueTestSuite() override = default;

protected:
    std::vector<int> collectionOfInts_{2, 6, 1, 5, 9};
    std::shared_ptr<Queue<int>> queue_;
};

TEST_F(QueueTestSuite, enqueue)
{
    for (auto i : collectionOfInts_)
    {
        queue_->enqueue(i);
    }
    EXPECT_EQ(2, queue_->peek());
    queue_->print();
}
TEST_F(QueueTestSuite, dequeue)
{
    for (auto i : collectionOfInts_)
    {
        queue_->enqueue(i);
    }
    for (auto i : collectionOfInts_)
    {
        EXPECT_EQ(i, queue_->dequeue());
    }
    EXPECT_TRUE(queue_->isEmpty());
}
TEST_F(QueueTestSuite, throwIfEmpty) { EXPECT_ANY_THROW(queue_->dequeue()); }
}  // namespace test
